        parameter(n_rbio=0,nflu=37,nSpf=3)! npl is numbers of bio reactions (flu)
        common /rbio/    flu(ndrei,nflu),Spf(ndrei,3:nbio,nSpf)
!     &                   i1_r(n_rbio),j1_r(n_rbio),
!     &                   Area(n_rbio),Dvolh(n_rbio,ilo),
!     &                   flu(ndrei,nflu),Spf(ndrei,3:nbio,nSpf)

*-------------BIOMASS, CONCENTRATION--------------------
c      ibio=2,nbio: biological state variables 
c all biological variables and fluxes used in the model
c are in mgC/m**3. Original input/output units: 
c      ibio=3 - flaggelates  [mgC/m**3]
c      ibio=4 - diatomes     [mgC/m**3]
c      ibio=5 - small zoo    [mgC/m**3]
c      ibio=6 - large zoo    [mgC/m**3]
c      ibio=7 - Detritus     [mgC/m**3]
c      ibio=8 - NH4          [mmolN/m**3]
c      ibio=9 - NO2          [mmolN/m**3]
c      ibio=10- NO3          [mmolN/m**3]
c      ibio=11- PO4          [mmolP/m**3]
c      ibio=12- SiO2         [mmolSi/m**3]
c      ibio=13- O2           [mml/m**3]
c      ibio=14- Opal         [mmolSi/m**3]
*--------------PRODUCTION--------------------------
c       l=1,nflu: biological fluxes of matter (units for output)  
c      l=1 - flaggelates production           [mgC/m**3 per day]
c      l=2 - diatomes  production             [mgC/m**3 per day]
c      l=3 - small zoo grazing on flaggelates [mgC/m**3 per day]
c      l=4 - small zoo grazing on diatomes    [mgC/m**3 per day]
c      l=5 - small zoo grazing on Detritus    [mgC/m**3 per day]
c      l=6 - large zoo grazing on flaggelates [mgC/m**3 per day]
c      l=7 - large zoo grazing on diatomes    [mgC/m**3 per day]
c      l=8 - large zoo grazing on small zoo   [mgC/m**3 per day]
c      l=9 - large zoo grazing on Detritus    [mgC/m**3 per day]

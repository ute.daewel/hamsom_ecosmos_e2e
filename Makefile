#SASCII start run from physics climatology
#SATLAS start run biology from WOCA ATlas values
#else remember defining the input file in the code
#.SUFFIXES: .f .F .F90 .f90 .o .mod
COMP=mpiifort -g -cpp -O2 -fp-model source -assume byterecl -DMPI -DMPIP
#COMP=mpiifort -g -traceback -check bounds -cpp -fp-model source -assume byterecl -mcmodel=medium -DMPIP -DMPI
OTHERFLAGS = $(shell /sw/rhel6-x64/netcdf/netcdf_fortran-4.4.3-intel14/bin/nf-config --fflags)
LIBFLAGS = $(shell /sw/rhel6-x64/netcdf/netcdf_fortran-4.4.3-intel14/bin/nf-config --flibs)
OBJ=ncio.o bio_ecosmoe2e.o hydrodynamics_dd_opt_4.o  ice_dynamics.o  setup_30m.o  subs.o  trmice_cd2swr.o linear.o parallel.o main_ecosmo.o
ECOSMO_t01.out :$(OBJ)
	$(COMP) $(OPT) $(OBJ) $(LIBFLAGS) -o ECOSMO_t01.out
clean: 
	rm $(OBJ) 
ncio.o: ncio.f90
	$(COMP) $(OTHERFLAGS) -c ./ncio.f90
bio_ecosmoe2e.o: bio_ecosmoe2e.F
	$(COMP) $(OPT) -c bio_ecosmoe2e.F
hydrodynamics_dd_opt_4.o: hydrodynamics_dd_opt_4.F
	$(COMP) $(OPT) -c hydrodynamics_dd_opt_4.F

ice_dynamics.o: ice_dynamics.F
	$(COMP) $(OPT) -c ice_dynamics.F

main_ecosmo.o: main_ecosmo.F
	$(COMP) $(OPT) $(OTHERFLAGS) -c  main_ecosmo.F

setup_30m.o : setup_30m.F
	$(COMP) $(OPT) -c setup_30m.F

subs.o : subs.F
	$(COMP) $(OPT) -c subs.F

trmice_cd2swr.o: trmice_cd2swr.F
	$(COMP) $(OPT) -c trmice_cd2swr.F

parallel.o: parallel.F
	$(COMP) $(OPT) -c parallel.F

linear.o: linear.F
	$(COMP) $(OPT) -c linear.F

co2_dyn.o: co2_dyn.F90
	$(COMP) $(OPT) -c co2_dyn.F90



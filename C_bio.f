c        common /bi/  BioC(55),BioOM(4,m,ilo),GI(4,6),
c     &               REDF(20),BWup(ilo),
c     &               sedy,QNH4,QBup
       real PrmBioC(65),PrmGI(4,6),PrmREDF(20),BioOM(4,m,ilo)
       real BioC(65),GI(4,6),REDF(20)
       common /params/ BioC,GI,REDF,BioOM
        parameter (sedy0=24.*60.*60.)

        common /bi3/  biolag,lbio,lgday,lsday,lgmth,lsmth
        common /bio5/ Dvoltj(m,ilo,n),
     &                DenBio(m,ilo),
     &                volbio,AreaH
c     &                volbio,AreaH,Bmass(nbio+1)

        common /bio_out/bio_out(ndrei,3:nbio+nsed)  !+3 sediment fields
        common /dswr/ daily_swr(m,n,2)
        REAL Dvoltj 
        Integer biolag

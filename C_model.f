*     c_model.f

c______ Model north_b _________________
      parameter (m=177,n=207,ilo=20,nxra=m*n)                  ! i,j,k model size
      parameter (khor=8216,ndrei=82108,kasor=8106)    ! 2d->1d, 3d->1d, 2d->1d for SOR
      parameter (lrp=55,iranz=2,lvrp=840)             ! liquid boudaries size
c      parameter (nbio=23)  !biological parameters, see C_bio.f
c      parameter (nbio=15)  !biological parameters, see C_bio.f
      parameter (nsed=4)  !number of sediment compartements, see C_bio.f
      parameter (nbiox=16)  !biological parameters, see C_bio.f
      parameter (nbio=nbiox)  !biological parameters, see C_bio.f
      parameter (ips=3,ipl=4,ibg=15,izs=5,izl=6,idet=7,inh4=8,
     &idom=9,ino3=10,ipo4=11,isio=12,io2=13,iop=14,ifi1=16,
     &ised1=1,ised2=2,ised3=3,imb1=4)
      parameter(idic=nbiox+1,ialk=nbiox+2,
     &ipco=nbiox+3,iph=nbiox+4,iatc=nbiox+5)  !field: bio
c Attention!!! ngro must be recalculated depending on model domain geometry by
c              ngro = (max(m*(ilo*20+10),(kasor*8)))
      parameter (ngro=m*(ilo*20+10)) ! north_b
c_____   north_b _________________
